import { createWebHistory, createRouter } from "vue-router";

import Home from "../views/ViewHome.vue";
import Table from "../views/Table.vue";

const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/home",
    component: Home,
  },
  {
    path: "/table",
    component: Table,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
